File manager for access to files on remote hosts by SSH. Also if remote host has Hadoop installation, work with Hadoop file system.

### Requirements
JRE 1.6 or higher pre installed.

### Existing binaries
1. Download latest version [here](https://sourceforge.net/projects/hadoopsshfilemanager/files/)
1. Create configuration in root directory:
    * Rename "connection.properties.template" to "connection.properties"
    * specify profiles with host name and credentials in "connection.properties"
    * specify active profile in "connection.properties"
1. Run command "bin/run.sh"

### Build from sources
1. Install [Maven utility](https://maven.apache.org/download.cgi)
1. Download sources
1. In sources root run command

```
mvn clean package
```

### License
APACHE LICENSE, VERSION 2.0


<img src="img/screenshot.jpeg">