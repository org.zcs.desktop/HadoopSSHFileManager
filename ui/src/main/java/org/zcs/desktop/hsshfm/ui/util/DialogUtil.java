package org.zcs.desktop.hsshfm.ui.util;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public final class DialogUtil {
	private static JFrame frame;

	private DialogUtil() {
	};

	public static void showMessage(final String message) {
		JOptionPane.showMessageDialog(frame, message);
	}

	public static void showException(final Exception e) {
		e.printStackTrace();
		JOptionPane.showMessageDialog(frame, e.getMessage(), "Error",
				JOptionPane.ERROR_MESSAGE);
	}

	public static boolean confirm(final String title, final String message) {
		return JOptionPane.showConfirmDialog(frame, message, title,
				JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION;
	}

	public static String input(final String title, final String initial) {
		return JOptionPane.showInputDialog(frame, title, initial);
	}

	public static void setFrame(JFrame frame) {
		DialogUtil.frame = frame;
	}

	public static JFrame getFrame() {
		return frame;
	}

}
