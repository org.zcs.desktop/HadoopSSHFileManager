package org.zcs.desktop.hsshfm.app;

import org.zcs.desktop.hsshfm.config.encryption.StringDecryptor;

import java.util.Arrays;

public class EncodeApp {

    public static void main(String[] argv) {
        StringDecryptor decryptor = new StringDecryptor();
        if (!decryptor.enabled()) System.exit(1);

        if (argv.length == 0) {
            System.out.println("Error: Plain password as parameter is required");
            System.exit(1);
        }
        Arrays.stream(argv).forEach(p -> System.out.println(decryptor.encrypt(p)));
    }
}
