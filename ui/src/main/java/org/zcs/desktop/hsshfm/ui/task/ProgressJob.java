package org.zcs.desktop.hsshfm.ui.task;

import org.zcs.desktop.hsshfm.ui.util.DialogUtil;

public abstract class ProgressJob<T> {
	public abstract T doInBackground() throws Exception;

	public void onSucess(T result) {
	}

	public void onError(Exception e) {
		DialogUtil.showException(e);		
	}
}
