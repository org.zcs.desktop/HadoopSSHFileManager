package org.zcs.desktop.hsshfm.ui.controller;

import org.zcs.desktop.hsshfm.model.NavigationItem;
import org.zcs.desktop.hsshfm.model.BundleProvider;
import org.zcs.desktop.hsshfm.service.BundleProviderFactory;
import org.zcs.desktop.hsshfm.service.fileoperation.FileOperations;
import org.zcs.desktop.hsshfm.ui.view.navpanel.NavigationPanelImpl;

public class NavigationPanelControllerDep {
	protected NavigationPanelImpl panel;

	protected BundleProvider provider;

	protected NavigationItem previousItem;

	protected BundleProviderFactory bundleFactory;

	protected DestinationProvider destinationProvider;

	protected FileOperations fileOperations;

	public void setPanel(NavigationPanelImpl panel) {
		this.panel = panel;
	}

	public void setProvider(BundleProvider provider) {
		this.provider = provider;
	}

	public void setBundleFactory(BundleProviderFactory bundleFactory) {
		this.bundleFactory = bundleFactory;
	}

	public void setDestinationProvider(DestinationProvider destinationProvider) {
		this.destinationProvider = destinationProvider;
	}

	public void setFileOperations(FileOperations fileOperations) {
		this.fileOperations = fileOperations;
	}

}
