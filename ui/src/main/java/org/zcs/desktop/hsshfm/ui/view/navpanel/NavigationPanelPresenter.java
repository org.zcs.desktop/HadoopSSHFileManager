package org.zcs.desktop.hsshfm.ui.view.navpanel;

import java.io.IOException;
import java.util.List;

import org.zcs.desktop.hsshfm.model.NavigationBundle;
import org.zcs.desktop.hsshfm.model.NavigationItem;

public interface NavigationPanelPresenter {
	void onItemEntered(final NavigationItem item) throws IOException;

	void onConnectionChanged(String connectionName) throws IOException;

	void onCopy(NavigationBundle navigationBundle,
			List<NavigationItem> itemList) throws IOException;

	void onDelete(NavigationBundle navigationBundle,
			List<NavigationItem> itemList) throws IOException;
	void onMkdir(NavigationBundle navigationBundle) throws IOException;
	void onBookmark(NavigationBundle navigationBundle);
	void onFocusGained();
	void onExtract(NavigationBundle navigationBundle, List<NavigationItem> itemList) throws IOException;
	void onLocationChanged(String path);
	
}