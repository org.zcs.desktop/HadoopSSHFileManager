package org.zcs.desktop.hsshfm.ui.view.navpanel;

import org.zcs.desktop.hsshfm.model.NavigationItem;
import org.zcs.desktop.hsshfm.ui.util.DialogUtil;

import javax.swing.*;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.*;
import java.util.Arrays;

public class NavPanelEventController {
	final NavigationPanelImpl panel;
	private static final String ENTER = "Enter";
	private static final String F5 = "F5";
	private static final String CONTROL_C = "CTRL-C";

	public NavPanelEventController(final NavigationPanelImpl panel) {
		this.panel = panel;
	}

	protected void addListeners() {
		panel.getTable().addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2) {
					onMouseDoubleClick();
				}
			}
		});

		assignKeystrokes();
		panel.getTable().addFocusListener(new FocusListener() {

			@Override
			public void focusGained(FocusEvent e) {
				panel.getTable().repaint();
			}

			@Override
			public void focusLost(FocusEvent e) {
				panel.getTable().repaint();
			}
		});

	}

	private void onMouseDoubleClick() {
		try {
			panel.getPresenter().onItemEntered(
					(NavigationItem) panel.getTableModel().getValueAt(
							panel.getTable().getSelectedRow(),
							panel.getTable().getSelectedColumn()));
		} catch (Exception e) {
			DialogUtil.showException(e);
		}
	}

	private void assignKeystrokes() {
		assignEnterKeystroke();
		assignF5Keystroke();
		assignTabKeystroke();
		assignF8Keystroke();
		assignF7Keystroke();
		assignBookmarkKeystroke();
		assignShiftF2Keystroke();
		assignCtrlCKeystroke();
	}
	private void assignCtrlCKeystroke(){
		KeyStroke controlC = KeyStroke.getKeyStroke(KeyEvent.VK_C, InputEvent.CTRL_MASK);
		KeyStroke controlIns = KeyStroke.getKeyStroke(KeyEvent.VK_INSERT, InputEvent.CTRL_MASK);
		panel.getTable().getInputMap(JTable.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT)
				.put(controlC, CONTROL_C);
		panel.getTable().getInputMap(JTable.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT)
				.put(controlIns, CONTROL_C);
		panel.getTable().getActionMap().put(CONTROL_C, new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				onCTRLCPressed();
			}
		});

	}

	private void onCTRLCPressed() {
		if (panel.getTable().getSelectedRowCount() == 0) {
			return;
		}
		final NavigationItemTableCellRenderer renderer = (NavigationItemTableCellRenderer) panel.getTable().getDefaultRenderer(NavigationItem.class);
		int[] selectedRows = panel.getTable().getSelectedRows();

		String selectedItemsString = Arrays.stream(selectedRows)
				.mapToObj(index -> getFormattedValue(renderer, index))
				.reduce((acc, curr) -> acc + System.lineSeparator() + curr).get();

		final Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		clipboard.setContents(new StringSelection(selectedItemsString), null);
	}

	private String getFormattedValue(final NavigationItemTableCellRenderer renderer, int index) {
		final StringBuilder result = new StringBuilder();
		final NavigationItem item = (NavigationItem) panel.getTableModel().getValueAt(index, 0);

		for (int i = 0; i < panel.getTable().getColumnCount(); i++) {
			result.append(renderer.getValue(item, i) + "\t");
		}
		return result.toString();
	}

	private void assignEnterKeystroke() {
		KeyStroke enter = KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0);
		panel.getTable().getInputMap(JTable.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT)
				.put(enter, ENTER);
		panel.getTable().getActionMap().put(ENTER, new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				onEnterPressed();
			}
		});
	}

	private void onEnterPressed() {
		if (panel.getTable().getSelectedRowCount() > 1
				|| panel.getTable().getSelectedRowCount() == 0) {
			return;
		}
		try {
			panel.getPresenter().onItemEntered(
					(NavigationItem) panel.getTableModel().getValueAt(
							panel.getTable().getSelectedRow(),
							panel.getTable().getSelectedColumn()));
		} catch (Exception e2) {
			e2.printStackTrace();
		}
	}

	private void assignF5Keystroke() {
		KeyStroke f5 = KeyStroke.getKeyStroke(KeyEvent.VK_F5, 0);
		panel.getTable().getInputMap(JTable.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT)
				.put(f5, F5);
		panel.getTable().getActionMap().put(F5, new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				onF5Pressed();
			}
		});
	}

	private void assignBookmarkKeystroke() {
		KeyStroke f5 = KeyStroke.getKeyStroke(KeyEvent.VK_D,
				InputEvent.CTRL_MASK);
		panel.getTable().getInputMap(JTable.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT)
				.put(f5, "CTRL_D");
		panel.getTable().getActionMap().put("CTRL_D", new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				panel.getPresenter().onBookmark(panel.getNavigationBundle());
			}
		});
	}

	private void assignF8Keystroke() {
		KeyStroke keyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_F8, 0);
		panel.getTable().getInputMap(JTable.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT)
				.put(keyStroke, keyStroke);
		panel.getTable().getActionMap().put(keyStroke, new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				onF8Pressed();
			}
		});
	}

	private void assignShiftF2Keystroke() {
		KeyStroke f5 = KeyStroke.getKeyStroke(KeyEvent.VK_F2,
				InputEvent.SHIFT_MASK);
		panel.getTable().getInputMap(JTable.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT)
				.put(f5, "SHIFT_F2");
		panel.getTable().getActionMap().put("SHIFT_F2", new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				onExtract();
			}
		});
	}

	private void onExtract() {
		try {
			panel.getPresenter().onExtract(panel.getNavigationBundle(),
					panel.getSelectedItems());
		} catch (Exception e) {
			DialogUtil.showException(e);
		}
	}

	private void assignF7Keystroke() {
		KeyStroke keyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_F7, 0);
		panel.getTable().getInputMap(JTable.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT)
				.put(keyStroke, keyStroke);
		panel.getTable().getActionMap().put(keyStroke, new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				onF7Pressed();
			}
		});
	}

	private void assignTabKeystroke() {
		final KeyStroke ks = KeyStroke.getKeyStroke(KeyEvent.VK_TAB, 0);
		Action nextTab = new AbstractAction("NextTab") {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent evt) {
				KeyboardFocusManager manager = KeyboardFocusManager
						.getCurrentKeyboardFocusManager();
				manager.focusNextComponent();
				panel.getTable().repaint();
			}
		};
		panel.getTable().getActionMap().put("NextTab", nextTab);
		panel.getTable()
				.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT)
				.put(ks, "NextTab");
	}

	private void onF5Pressed() {
		if (panel.getTable().getSelectedRowCount() == 0) {
			return;
		}
		try {
			panel.getPresenter().onCopy(panel.getNavigationBundle(),
					panel.getSelectedItems());
		} catch (Exception e) {
			e.printStackTrace();
			DialogUtil.showMessage("Error: " + e.getMessage());
		}
	}

	private void onF8Pressed() {
		if (panel.getTable().getSelectedRowCount() == 0) {
			return;
		}
		try {
			panel.getPresenter().onDelete(panel.getNavigationBundle(),
					panel.getSelectedItems());
		} catch (Exception e) {
			e.printStackTrace();
			DialogUtil.showMessage("Error: " + e.getMessage());
		}
	}

	private void onF7Pressed() {
		try {
			panel.getPresenter().onMkdir(panel.getNavigationBundle());
		} catch (Exception e) {
			e.printStackTrace();
			DialogUtil.showMessage("Error: " + e.getMessage());
		}
	}

}
