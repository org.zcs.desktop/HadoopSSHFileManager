package org.zcs.desktop.hsshfm.ui.view;

import com.google.inject.Inject;
import org.zcs.desktop.hsshfm.service.BookmarkService;
import org.zcs.desktop.hsshfm.service.BundleProviderFactory;
import org.zcs.desktop.hsshfm.service.fileoperation.FileOperations;
import org.zcs.desktop.hsshfm.ui.controller.NavigationPanelController;
import org.zcs.desktop.hsshfm.ui.controller.SelectedController;
import org.zcs.desktop.hsshfm.ui.view.navpanel.NavigationPanelImpl;

public class TwoPanelBuilder {
	private NavigationPanelImpl leftPanel;
	private NavigationPanelImpl rightPanel;
	private BundleProviderFactory bundleFactory;
	private final FileOperations fileOperations = new FileOperations();
	private BookmarkService bookmarkService;
	private SelectedController selectedController;

	@Inject
	public TwoPanelBuilder(NavigationPanelImpl leftPanel, NavigationPanelImpl rightPanel) {
		this.leftPanel = leftPanel;
		this.rightPanel = rightPanel;
	}

	public void build(final BundleProviderFactory bundleFactory) {
		this.bundleFactory = bundleFactory;

		final NavigationPanelController leftController = createController(leftPanel);
		leftPanel.setPresenter(leftController);

		final NavigationPanelController rightController = createController(rightPanel);
		rightPanel.setPresenter(rightController);

		leftController.setDestinationProvider(rightController);
		rightController.setDestinationProvider(leftController);

		fileOperations.setBundleFactory(bundleFactory);

		initController(leftController);
		initController(rightController);
	}

	void initController(final NavigationPanelController controller) {
		try {
			controller.init();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}

	private NavigationPanelController createController(
			final NavigationPanelImpl panel) {
		final NavigationPanelController result = new NavigationPanelController();
		result.setBundleFactory(bundleFactory);
		result.setPanel(panel);
		result.setFileOperations(fileOperations);
		result.setBookmarkService(bookmarkService);
		result.setSelectedController(selectedController);
		return result;
	}

	public NavigationPanelImpl getLeftPanel() {
		return leftPanel;
	}

	public NavigationPanelImpl getRightPanel() {
		return rightPanel;
	}

	public void setBookmarkService(BookmarkService bookmarkService) {
		this.bookmarkService = bookmarkService;
	}

	public void setSelectedController(SelectedController selectedController) {
		this.selectedController = selectedController;
	}

}
