package org.zcs.desktop.hsshfm.app;

import com.google.inject.Guice;
import com.google.inject.Injector;
import org.zcs.desktop.hsshfm.init.UIModule;
import org.zcs.desktop.hsshfm.ui.view.MainFrame;

public class Application {

    public static void main(String[] args) {
        final Injector injector = Guice.createInjector(new UIModule());
        final MainFrame mainFrame = injector.getInstance(MainFrame.class);
        mainFrame.setVisible(true);
    }

}
