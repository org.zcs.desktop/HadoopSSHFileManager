package org.zcs.desktop.hsshfm.ui.view;

import java.awt.BorderLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JSplitPane;

import com.google.inject.Inject;
import org.zcs.desktop.hsshfm.service.BookmarkService;
import org.zcs.desktop.hsshfm.service.BundleProviderFactory;
import org.zcs.desktop.hsshfm.ui.controller.SelectedController;
import org.zcs.desktop.hsshfm.ui.util.DialogUtil;

public class MainFrame extends JFrame {
	private static final long serialVersionUID = 1L;
	private final BundleProviderFactory bundleFactory;
	private final BookmarkService bookmarkService = new BookmarkService();
	private TwoPanelBuilder panelBuilder;
	private SelectedController selectedController = new SelectedController();
	private static final String HELP_STRING = "F5-copy, F7-MkDir, F8-Rm, CTL-D Bookmark, Shift-F2 Extract files";

	private final JLabel helpLabel = new JLabel(HELP_STRING);

	@Inject
	public MainFrame(final TwoPanelBuilder panelBuilder, final BundleProviderFactory bundleFactory) {
		super("Hadoop SSH File manager");
		this.panelBuilder = panelBuilder;
		this.bundleFactory = bundleFactory;
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		getContentPane().setLayout(new BorderLayout());
		getContentPane().add(createSplitPane(), BorderLayout.CENTER);
		getContentPane().add(helpLabel, BorderLayout.SOUTH);

		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosed(WindowEvent e) {
				bundleFactory.close();
			}
		});
		DialogUtil.setFrame(this);
		setJMenuBar(createMainMenu());
		pack();
	}

	private final JMenuBar createMainMenu() {
		final MainFrameMenu result = new MainFrameMenu();
		result.setBookmarkService(bookmarkService);
		final MainFramePresenterImpl presenter = new MainFramePresenterImpl();
		presenter.setSelectedController(selectedController);
		result.setPresenter(presenter);
		result.createUI();

		return result;
	}

	private JSplitPane createSplitPane() {
		panelBuilder.setBookmarkService(bookmarkService);
		panelBuilder.setSelectedController(selectedController);
		panelBuilder.build(bundleFactory);
		setFocusTraversalPolicy(new TwoPanelsFocusTraversalPolicy(panelBuilder
				.getLeftPanel().getTable(), panelBuilder.getRightPanel()
				.getTable()));

		final JSplitPane result = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,
				panelBuilder.getLeftPanel(), panelBuilder.getRightPanel());
		result.setDividerLocation(0.5);

		panelBuilder = null;

		return result;
	}

}
