package org.zcs.desktop.hsshfm.ui.view.navpanel;

import java.util.Collections;
import java.util.List;

import javax.swing.table.DefaultTableModel;

import org.zcs.desktop.hsshfm.model.NavigationItem;

public class NavigationTableModel extends DefaultTableModel {

	private static final long serialVersionUID = 1L;
	private static final String[] COLUMN_NAMES = { "Name", "Size", "Changed" };

	private List<NavigationItem> items = Collections.emptyList();

	@Override
	public int getRowCount() {
		if (items == null) {
			return 0;
		}
		return items.size();
	}

	@Override
	public int getColumnCount() {
		return COLUMN_NAMES.length;
	}

	@Override
	public String getColumnName(int column) {
		return COLUMN_NAMES[column];
	}

	@Override
	public Object getValueAt(int row, int column) {
		return items.get(row);
	}

	@Override
	public boolean isCellEditable(int row, int column) {
		return false;
	}

	public void setItems(final List<NavigationItem> itemList) {
		this.items = itemList;
		fireTableDataChanged();
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return NavigationItem.class;
	}

}
