package org.zcs.desktop.hsshfm.ui.view.navpanel;

import com.google.inject.Inject;
import org.zcs.desktop.hsshfm.config.ConnectionNames;
import org.zcs.desktop.hsshfm.model.FileSystemType;
import org.zcs.desktop.hsshfm.model.NavigationBundle;
import org.zcs.desktop.hsshfm.model.NavigationItem;
import org.zcs.desktop.hsshfm.ui.util.DialogUtil;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;

public class NavigationPanelImpl extends JPanel {
    private static final long serialVersionUID = 1L;

    private NavigationPanelPresenter presenter;
    private NavigationTableModel tableModel;
    private JTable table;
    private final JLabel pathLabel = new JLabel();
    private final JComboBox connectionTypeComboBox = new JComboBox();

    private boolean isComboBoxLocked = false;

    private NavigationBundle navigationBundle;

    private final NavPanelEventController eventController = new NavPanelEventController(this);
    private final ConnectionNames connectionNames;

    @Inject
    public NavigationPanelImpl(final ConnectionNames connectionNames) {
        this.connectionNames = connectionNames;
        createUI();
    }

    public void createUI() {
        setLayout(new BorderLayout());
        tableModel = new NavigationTableModel();
        table = new JTable(tableModel);
        table.setSelectionMode(DefaultListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        table.setDefaultRenderer(NavigationItem.class, new NavigationItemTableCellRenderer());

        eventController.addListeners();

        add(createTopPanel(), BorderLayout.NORTH);
        add(new JScrollPane(table), BorderLayout.CENTER);
        table.addFocusListener(new FocusListener() {
            @Override
            public void focusLost(FocusEvent e) {

            }

            @Override
            public void focusGained(FocusEvent e) {
                presenter.onFocusGained();
            }
        });
        pathLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    String originalPath = pathLabel.getText();
                    String updatedPath = DialogUtil.input("Label", pathLabel.getText());
                    if (updatedPath != null && !updatedPath.equals(originalPath)) {
                        presenter.onLocationChanged(updatedPath);
                    }
                }
            }
        });
    }

    protected List<NavigationItem> getSelectedItems() {
        final List<NavigationItem> itemList = new ArrayList<NavigationItem>();
        for (Integer current : table.getSelectedRows()) {
            itemList.add(getItem(current));
        }
        return itemList;
    }

    private NavigationItem getItem(final int rowno) {
        return (NavigationItem) tableModel.getValueAt(rowno, table.getSelectedColumn());
    }

    public void setPresenter(NavigationPanelPresenter presenter) {
        this.presenter = presenter;
    }

    public void selectItem(final NavigationItem item) {
        for (int i = 0; i < navigationBundle.getItemList().size(); i++) {
            if (navigationBundle.getItemList().get(i).equals(item)) {
                table.getSelectionModel().setSelectionInterval(i, i);
            }
        }
    }

    public void setNavigationBundle(final NavigationBundle navigationBundle) {
        this.navigationBundle = navigationBundle;
        tableModel.setItems(navigationBundle.getItemList());
        table.getSelectionModel().setSelectionInterval(0, 0);
        pathLabel.setText(getPath(navigationBundle));
    }

    private JPanel createTopPanel() {
        final JPanel result = new JPanel();
        result.setLayout(new FlowLayout(FlowLayout.LEFT));
        result.add(createFileTypeComboBox());
        result.add(pathLabel);
        return result;
    }

    private String getPath(final NavigationBundle navigationBundle) {
        switch (navigationBundle.getBundleProvider().getFileSystemType()) {
        case SSH:
            return "SSH:" + navigationBundle.getPath();
        case HADOOP:
            return "Hadoop:" + navigationBundle.getPath();
        default:
            return navigationBundle.getPath();
        }
    }

    private JComboBox createFileTypeComboBox() {
        connectionNames.getNames().stream().forEach(name -> connectionTypeComboBox.addItem(name));

        connectionTypeComboBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (isComboBoxLocked) {
                    return;
                }
                try {
                    presenter.onConnectionChanged(connectionTypeComboBox.getSelectedItem().toString());
                } catch (Exception e2) {
                    DialogUtil.showMessage("Error: " + e2.getMessage());
                }
            }
        });
        return connectionTypeComboBox;
    }

    public void setFileSystemType(final FileSystemType fsType) {
        isComboBoxLocked = true;
        connectionTypeComboBox.setSelectedItem(fsType);
        isComboBoxLocked = false;
    }

    public NavigationBundle getNavigationBundle() {
        return navigationBundle;
    }

    public JTable getTable() {
        return table;
    }

    public NavigationPanelPresenter getPresenter() {
        return presenter;
    }

    public NavigationTableModel getTableModel() {
        return tableModel;
    }
}
