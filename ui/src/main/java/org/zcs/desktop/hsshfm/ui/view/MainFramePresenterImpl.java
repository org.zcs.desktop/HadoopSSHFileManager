package org.zcs.desktop.hsshfm.ui.view;

import org.zcs.desktop.hsshfm.model.Bookmark;
import org.zcs.desktop.hsshfm.ui.controller.SelectedController;

public class MainFramePresenterImpl implements MainFrameMenu.MainFramePresenter {
	private SelectedController selectedController;

	@Override
	public void onBookmarkSelected(Bookmark bookmark) {
		selectedController.getSelected().navigate(bookmark.getFileSystemType(),
				bookmark.getPath());
	}

	public void setSelectedController(SelectedController selectedController) {
		this.selectedController = selectedController;
	}

}
