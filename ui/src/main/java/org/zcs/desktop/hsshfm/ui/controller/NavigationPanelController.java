package org.zcs.desktop.hsshfm.ui.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.zcs.desktop.hsshfm.model.Bookmark;
import org.zcs.desktop.hsshfm.model.FileSystemType;
import org.zcs.desktop.hsshfm.model.NavigationBundle;
import org.zcs.desktop.hsshfm.model.NavigationItem;
import org.zcs.desktop.hsshfm.service.BookmarkService;
import org.zcs.desktop.hsshfm.ui.task.ProgressJob;
import org.zcs.desktop.hsshfm.ui.task.ProgressTask;
import org.zcs.desktop.hsshfm.ui.util.DialogUtil;
import org.zcs.desktop.hsshfm.ui.view.navpanel.NavigationPanelPresenter;

public class NavigationPanelController extends NavigationPanelControllerDep
		implements NavigationPanelPresenter, DestinationProvider {

	private BookmarkService bookmarkService;
	private SelectedController selectedController;

	// private final static Logger LOGGER = Logger
	// .getLogger(NavigationPanelController.class);

	public void init() throws IOException {
		provider = bundleFactory.getDefaultProvider();
		execJob(new SetPanelBundleJob() {
			@Override
			public NavigationBundle doInBackground() throws Exception {
				return provider.getRootNavigationBundle();
			}
		});
	}

	@Override
	public void onItemEntered(final NavigationItem item) throws IOException {
		if (provider.isExpandable(item)) {
			execJob(new ProgressJob<NavigationBundle>() {
				@Override
				public NavigationBundle doInBackground() throws Exception {
					return provider.getNavigationBundle(item);
				}

				public void onSucess(NavigationBundle result) {
					panel.setNavigationBundle(result);
					if (previousItem != null) {
						panel.selectItem(previousItem);
					}
					previousItem = item;
				}

			});

		}
	}

	@Override
	public void onConnectionChanged(final String connectionName) throws IOException {
		provider = bundleFactory.getProvider(connectionName);
		execJob(new SetPanelBundleJob() {
			@Override
			public NavigationBundle doInBackground() throws Exception {
				return provider.getRootNavigationBundle();
			}
		});
	}

	@Override
	public void onCopy(final NavigationBundle navigationBundle,
			final List<NavigationItem> itemList) throws IOException {
		if (!DialogUtil.confirm("Copy", "Copy " + itemList.size()
				+ " files from \n\"" + navigationBundle.getPath()
				+ "\" to \n\"" + destinationProvider.getBundle().getPath()
				+ "\"?")) {
			return;
		}
        final List<String> existingFileNames = getExistingFileNames(itemList, destinationProvider.getBundle());
        if (!existingFileNames.isEmpty()
                && !DialogUtil.confirm("Warning", String.format("Overwrite %s files\n%s\n?", existingFileNames.size(), existingFileNames))) {
            return;
        }
		execJob(new ProgressJob<Void>() {
			@Override
			public Void doInBackground() throws Exception {
				fileOperations.copy(navigationBundle, itemList,
						destinationProvider.getBundle());
				destinationProvider.refresh();
				return null;
			}
		});

	}
	
    private List<String> getExistingFileNames(final List<NavigationItem> itemList, final NavigationBundle destinationBundle) {
        final List<String> result = new ArrayList<String>();
        for (final NavigationItem current : itemList) {
            if (contains(destinationBundle, current.getName())) {
                result.add(current.getName());
            }
        }
        return result;
    }

    private boolean contains(final NavigationBundle bundle, final String name) {
        for (final NavigationItem current : bundle.getItemList()) {
            if (current.getName().equals(name)) {
                return true;
            }
        }
        return false;
    }

	@Override
	public NavigationBundle getBundle() {
		return panel.getNavigationBundle();
	}

	abstract class SetPanelBundleJob extends ProgressJob<NavigationBundle> {
		@Override
		public void onSucess(NavigationBundle result) {
			panel.setNavigationBundle(result);
		}
	}

	private <V> void execJob(ProgressJob<V> task) {
		new ProgressTask<V>(task).execute();

	}

	@Override
	public void refresh() {
		execJob(new SetPanelBundleJob() {
			@Override
			public NavigationBundle doInBackground() throws Exception {
				return provider.refresh(panel.getNavigationBundle().getPath());
			}
		});
	}

	@Override
	public void onDelete(final NavigationBundle navigationBundle,
			final List<NavigationItem> itemList) throws IOException {
		if (!DialogUtil.confirm("Delete", "Delete " + itemList.size()
				+ " files from \n\"" + navigationBundle.getPath() + "\"?")) {
			return;
		}
		execJob(new ProgressJob<Void>() {
			@Override
			public Void doInBackground() throws Exception {
				fileOperations.rm(navigationBundle, itemList);
				refresh();
				return null;
			}
		});

	}

	@Override
	public void onMkdir(final NavigationBundle navigationBundle)
			throws IOException {
		final String name = DialogUtil.input("Enter name", "");
		if (name == null) {
			return;
		}
		execJob(new ProgressJob<Void>() {
			@Override
			public Void doInBackground() throws Exception {
				fileOperations.mkdir(navigationBundle, name);
				refresh();
				return null;
			}
		});
	}

	@Override
	public void onBookmark(final NavigationBundle navigationBundle) {
		if (DialogUtil.confirm("Create Bookmark", "Bookmark "
				+ navigationBundle.getPath() + " ?")) {
			final Bookmark bookmark = new Bookmark(navigationBundle.getBundleProvider().getFileSystemType(),
					navigationBundle.getPath());
			try {
				bookmarkService.add(bookmark);
			} catch (Exception e) {
				DialogUtil.showException(e);
			}
		}
	}

	public void navigate(final FileSystemType fsType, final String path) {
		if (!fsType.equals(provider.getFileSystemType())) {
			// FIXME
//			provider = bundleFactory.getProvider(fsType);
		}
		panel.setFileSystemType(fsType);
		execJob(new SetPanelBundleJob() {
			@Override
			public NavigationBundle doInBackground() throws Exception {
				return provider.getNavigationBundle(path);
			}
		});
	}

	@Override
	public void onExtract(final NavigationBundle navigationBundle,
			final List<NavigationItem> itemList) throws IOException {
		boolean isExtracted = false;
		for (final NavigationItem current : itemList) {
			isExtracted |= provider.isExtractable(current);
		}
		if (isExtracted && DialogUtil.confirm("Extract", "Extract files?")) {
			execJob(new ProgressJob<Void>() {
				@Override
				public Void doInBackground() throws Exception {
					provider.extract(navigationBundle, itemList);
					return null;
				}
			});
		}
	}

	@Override
	public void onLocationChanged(String path) {
		navigate(panel.getNavigationBundle().getBundleProvider().getFileSystemType(), path);
	}

	@Override
	public void onFocusGained() {
		selectedController.setController(this);
	}

	public void setBookmarkService(BookmarkService bookmarkService) {
		this.bookmarkService = bookmarkService;
	}

	public void setSelectedController(SelectedController selectedController) {
		this.selectedController = selectedController;
	}

}
