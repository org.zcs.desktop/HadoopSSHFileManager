package org.zcs.desktop.hsshfm.ui.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;

import org.zcs.desktop.hsshfm.model.Bookmark;
import org.zcs.desktop.hsshfm.service.BookmarkService;
import org.zcs.desktop.hsshfm.ui.util.DialogUtil;

public class MainFrameMenu extends JMenuBar {
	public interface MainFramePresenter {
		void onBookmarkSelected(Bookmark bookmark);
	}

	private BookmarkService bookmarkService;

	private static final long serialVersionUID = 1L;
	private MainFramePresenter presenter;

	public void createUI() {
		add(getFileMenu());
	}

	private JMenu getFileMenu() {
		final JMenu result = new JMenu("Bookmarks");
		result.setMnemonic(KeyEvent.VK_F);
		result.addMenuListener(new MenuListener() {

			@Override
			public void menuSelected(MenuEvent e) {
				System.out.println(DialogUtil.getFrame().getFocusTraversalPolicy());
				appendMenuItems(result);
			}

			@Override
			public void menuDeselected(MenuEvent e) {
			}

			@Override
			public void menuCanceled(MenuEvent e) {

			}
		});
		/*
		 * //--*- import -*-- JMenuItem importItem = new JMenuItem(IMPORT);
		 * importItem.setActionCommand(IMPORT);
		 * importItem.addActionListener(this); result.add(importItem);
		 * result.addSeparator(); // --*- exit -*-- JMenuItem menuItem = new
		 * JMenuItem(FILE_EXIT); menuItem.setMnemonic(KeyEvent.VK_E);
		 * menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E,
		 * ActionEvent.ALT_MASK)); menuItem.setActionCommand(FILE_EXIT);
		 * menuItem.addActionListener(this);
		 * 
		 * result.add(menuItem);
		 */
		return result;
	}

	private void appendMenuItems(final JMenu result) {
		result.removeAll();
		try {
			for (final Bookmark current : bookmarkService.list()) {
				final JMenuItem item = new JMenuItem(current.toString());
				item.addActionListener(new MenuActionListener(current));
				result.add(item);
			}
		} catch (Exception e1) {
			throw new RuntimeException(e1);
		}
	}

	public class MenuActionListener implements ActionListener {
		private final Bookmark bookmark;

		public MenuActionListener(final Bookmark bookmark) {
			this.bookmark = bookmark;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			presenter.onBookmarkSelected(bookmark);
		}

	}

	public void setBookmarkService(BookmarkService bookmarkService) {
		this.bookmarkService = bookmarkService;
	}

	public void setPresenter(MainFramePresenter presenter) {
		this.presenter = presenter;
	}

}
