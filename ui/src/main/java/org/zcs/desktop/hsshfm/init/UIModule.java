package org.zcs.desktop.hsshfm.init;

import com.google.inject.AbstractModule;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigBeanFactory;
import com.typesafe.config.ConfigFactory;
import org.zcs.desktop.hsshfm.config.ApplicationConfig;
import org.zcs.desktop.hsshfm.config.encryption.ApplicationConfigDecryptor;
import org.zcs.desktop.hsshfm.config.encryption.StringDecryptor;

import java.io.File;

public class UIModule extends AbstractModule {
    public static final String CONFIG_FILE_NAME = "application.conf";
    private final ApplicationConfig applicationConfig;

    public UIModule() {
        final Config originalConfig = getOriginalConfig();
        ApplicationConfig encryptedConfig = ConfigBeanFactory.create(originalConfig, ApplicationConfig.class);
        final ApplicationConfigDecryptor decryptor = new ApplicationConfigDecryptor(new StringDecryptor());
        this.applicationConfig = decryptor.decrypt(encryptedConfig);
    }

    protected void configure() {
        bind(ApplicationConfig.class).toInstance(applicationConfig);
    }

    private Config getOriginalConfig() {
        final File configFile = new File(CONFIG_FILE_NAME);
        final Config originalConfig;
        if (configFile.exists()) {
            originalConfig = ConfigFactory.parseFile(new File(CONFIG_FILE_NAME));
        } else {
            originalConfig = ConfigFactory.empty();
        }
        return originalConfig;
    }
}
