package org.zcs.desktop.hsshfm.ui.controller;

import org.zcs.desktop.hsshfm.model.NavigationBundle;

public interface DestinationProvider {
	public NavigationBundle getBundle();
	public void refresh();
}
