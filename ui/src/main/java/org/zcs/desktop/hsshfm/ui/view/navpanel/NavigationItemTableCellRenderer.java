package org.zcs.desktop.hsshfm.ui.view.navpanel;

import java.awt.Component;
import java.io.File;
import java.text.DateFormat;

import javax.swing.Icon;
import javax.swing.JTable;
import javax.swing.filechooser.FileSystemView;
import javax.swing.table.DefaultTableCellRenderer;

import org.zcs.desktop.hsshfm.model.NavigationItem;

public class NavigationItemTableCellRenderer extends DefaultTableCellRenderer {

	private static final long serialVersionUID = 1L;
	private final DateFormat dateFormat = DateFormat.getInstance();
	private final Icon folderIcon;

	public NavigationItemTableCellRenderer() {
		final String userDir = System.getProperty("user.dir");
		folderIcon = FileSystemView.getFileSystemView().getSystemIcon(
				new File(userDir));
	}

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value,
			boolean isSelected, boolean hasFocus, int row, int column) {
		super.getTableCellRendererComponent(table, value, table.isFocusOwner()
				&& isRowSelected(table, row) && isSelected, false, row, column);

		final NavigationItem item = (NavigationItem) value;
		/*
		 * Font f = getFont(); if(item.isFolder()){
		 * setFont(f.deriveFont(f.getStyle() | Font.BOLD)); } else {
		 * setFont(f.deriveFont(f.getStyle() & ~Font.BOLD)); }
		 */
		final boolean iconRequired=column == 0 && item.isFolder(); 

		if (iconRequired) {
			setIcon(folderIcon);
			setText(getValue(item, column));			
		}
		else {
			setIcon(null);
			setText("    "+getValue(item, column));			
		}
		return this;
	}

	private boolean isRowSelected(final JTable table, final int row) {
		for (int current : table.getSelectedRows()) {
			if (current == row) {
				return true;
			}
		}
		return false;
	}

	String getValue(final NavigationItem value, final int column) {
		final String result;
		switch (column) {
		case 0:
			result = value.getName();
			break;
		case 1:
			if (value.isFolder()) {
				result = "<folder>";
			} else {
				result = Long.toString(value.getSize());
			}
			break;
		default:
			if (value.getChanged() != null) {
				result = dateFormat.format(value.getChanged());
			} else {
				result = "";
			}
		}
		return result;
	}

}
