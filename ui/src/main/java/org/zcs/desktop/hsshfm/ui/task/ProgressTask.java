package org.zcs.desktop.hsshfm.ui.task;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.concurrent.TimeUnit;

import javax.swing.ProgressMonitor;
import javax.swing.SwingWorker;

import org.zcs.desktop.hsshfm.ui.util.DialogUtil;

public class ProgressTask<T> extends SwingWorker<T, Void> {
	private ProgressMonitor progressMonitor;
	private final ProgressJob<T> job;

	public ProgressTask(final ProgressJob<T> job) {
		this.job = job;
		addPropertyChangeListener(new ProgressPropertyChangeListener());
	}

	@Override
	protected T doInBackground() throws Exception {
		return job.doInBackground();
	}

	@Override
	protected void done() {
		try {
			job.onSucess(get());
		} catch (Exception e) {
			job.onError(e);
		}
	}

	class ProgressPropertyChangeListener implements PropertyChangeListener {

		@Override
		public void propertyChange(PropertyChangeEvent evt) {
			if (evt.getNewValue().equals(StateValue.STARTED)) {
				progressMonitor = new ProgressMonitor(DialogUtil.getFrame(),
						"Please wait..", "", 0, 100);
				progressMonitor.setMillisToPopup(-1);
				progressMonitor.setMillisToDecideToPopup(-1);
				new Thread(() -> {
					try {
						TimeUnit.MILLISECONDS.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					if (progressMonitor != null) {
						progressMonitor.setProgress(30);
					}
				}).start();

			} else if (evt.getNewValue().equals(StateValue.DONE)
					&& progressMonitor != null) {
				progressMonitor.close();
				progressMonitor = null;
			}

		}
	}

}
