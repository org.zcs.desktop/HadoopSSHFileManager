package org.zcs.desktop.hsshfm.config.connection;

import com.typesafe.config.Optional;

public class SSHConnectionConfig extends NamedConnection {
    private String host;
    @Optional
    private int port = 22;
    @Optional
    private String proxyHost;
    @Optional
    private int proxyPort;
    private String user;
    private String password;
    @Optional
    private String pkeyPath;

    public SSHConnectionConfig() {
    }

    public SSHConnectionConfig(String name, String host, int port, String proxyHost, int proxyPort, String user, String password, String pkeyPath) {
        setName(name);
        this.host = host;
        this.port = port;
        this.proxyHost = proxyHost;
        this.proxyPort = proxyPort;
        this.user = user;
        this.password = password;
        this.pkeyPath = pkeyPath;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPkeyPath() {
        return pkeyPath;
    }

    public void setPkeyPath(String pkeyPath) {
        this.pkeyPath = pkeyPath;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getProxyHost() {
        return proxyHost;
    }

    public void setProxyHost(String proxyHost) {
        this.proxyHost = proxyHost;
    }

    public int getProxyPort() {
        return proxyPort;
    }

    public void setProxyPort(int proxyPort) {
        this.proxyPort = proxyPort;
    }
}
