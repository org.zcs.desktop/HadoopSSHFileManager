package org.zcs.desktop.hsshfm.config.encryption;

import org.zcs.desktop.hsshfm.config.ApplicationConfig;
import org.zcs.desktop.hsshfm.config.connection.HDFSConnectionConfig;
import org.zcs.desktop.hsshfm.config.connection.SSHConnectionConfig;

import java.util.List;
import java.util.stream.Collectors;

public class ApplicationConfigDecryptor {
    private final StringDecryptor decryptor;

    public ApplicationConfigDecryptor(final StringDecryptor decryptor) {
        this.decryptor = decryptor;
    }

    public ApplicationConfig decrypt(ApplicationConfig encrypted) {
        return new ApplicationConfig(decryptSSH(encrypted.getSshConnections()), decryptHDFS(encrypted.getHdfsConnections()));
    }

    private List<HDFSConnectionConfig> decryptHDFS(List<HDFSConnectionConfig> encrypted) {
        return encrypted.stream().map(conn ->
                new HDFSConnectionConfig(
                        conn.getName(),
                        conn.getConfigFolder(),
                        conn.getKeytabFile(),
                        decryptor.decrypt(conn.getPrincipal()),
                        conn.isKrb5_debug()
                )
        ).collect(Collectors.toList());
    }

    private List<SSHConnectionConfig> decryptSSH(List<SSHConnectionConfig> encrypted) {
        return encrypted
                .stream()
                .map(conn -> new SSHConnectionConfig(
                        conn.getName(),
                        conn.getHost(),
                        conn.getPort(),
                        conn.getProxyHost(),
                        conn.getProxyPort(),
                        conn.getUser(),
                        decryptor.decrypt(conn.getPassword()),
                        conn.getPkeyPath()))
                .collect(Collectors.toList());
    }
}
