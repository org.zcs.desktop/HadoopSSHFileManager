package org.zcs.desktop.hsshfm.config.encryption;

import org.apache.commons.lang.StringUtils;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringDecryptor {
    final static String ENV_VARIABLE_NAME = "HSSHFM_ENCRYPTOR_PASSWORD";
    final Pattern encryptedPattern = Pattern.compile("ENC\\((.+)\\)");
    final StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();

    public StringDecryptor() {
        encryptor.setPassword(System.getenv(ENV_VARIABLE_NAME));
    }

    public String decrypt(String value) {
        final String result;
        if (isEncrypted(value)) {
            final Matcher matcher = encryptedPattern.matcher(value);
            matcher.find();
            String encoded = matcher.group(1);
            result = encryptor.decrypt(encoded);
        } else {
            result = value;
        }
        return result;
    }

    private boolean isEncrypted(String value) {
        return encryptedPattern.matcher(value).matches();
    }

    public boolean enabled() {
        boolean result = StringUtils.isNotBlank(System.getenv(ENV_VARIABLE_NAME));
        if (!result) {
            System.out.println("Not enabled");
            System.out.println(String.format("Please setup environment variable %s with password for enable encryption", ENV_VARIABLE_NAME));
        }
        return result;
    }

    public String encrypt(String value) {
        return encryptor.encrypt(value);
    }

}
