package org.zcs.desktop.hsshfm.config;

import com.google.inject.Inject;
import org.zcs.desktop.hsshfm.config.connection.HDFSConnectionConfig;
import org.zcs.desktop.hsshfm.config.connection.SSHConnectionConfig;
import org.zcs.desktop.hsshfm.model.FileSystemType;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ConnectionNames {
    private final ApplicationConfig applicationConfig;

    @Inject
    public ConnectionNames(final ApplicationConfig applicationConfig) {
        this.applicationConfig = applicationConfig;
    }

    public List<String> getNames() {
        final Stream<String> localNameStream = Stream.of(FileSystemType.LOCAL.name());
        final Stream<String> sshNames = applicationConfig.getSshConnections().stream().map(c -> c.getName());
        final Stream<String> hdfsNames = applicationConfig.getHdfsConnections().stream().map(c -> c.getName());
        return Stream.concat(localNameStream, Stream.concat(sshNames, hdfsNames)).collect(Collectors.toList());
    }

    public FileSystemType getType(String name) {
        if (FileSystemType.LOCAL.toString().equals(name)) {
            return FileSystemType.LOCAL;
        }
        if (applicationConfig.getSshConnections().stream().filter(c -> c.getName().equals(name)).count() > 0) {
            return FileSystemType.SSH;
        }
        if (applicationConfig.getHdfsConnections().stream().filter(c -> c.getName().equals(name)).count() > 0) {
            return FileSystemType.HADOOP;
        }
        throw new IllegalStateException(String.format("Connections with name %name does not exists!", name));
    }

    public SSHConnectionConfig getSSHConnectionConfig(String name) {
        return applicationConfig.getSshConnections().stream().filter(c -> c.getName().equals(name)).findFirst().get();
    }

    public HDFSConnectionConfig getHDFSConnectionConfig(String name) {
        return applicationConfig.getHdfsConnections().stream().filter(c -> c.getName().equals(name)).findFirst().get();
    }

}
