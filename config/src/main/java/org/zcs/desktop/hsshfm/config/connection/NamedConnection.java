package org.zcs.desktop.hsshfm.config.connection;

import java.io.Serializable;

public abstract class NamedConnection implements Serializable {
    private String name;
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
