package org.zcs.desktop.hsshfm.config;

import org.zcs.desktop.hsshfm.config.connection.HDFSConnectionConfig;
import org.zcs.desktop.hsshfm.config.connection.SSHConnectionConfig;

import java.io.Serializable;
import java.util.List;

public class ApplicationConfig implements Serializable {
    private List<SSHConnectionConfig> sshConnections;
    private List<HDFSConnectionConfig> hdfsConnections;

    public ApplicationConfig() {
    }

    public ApplicationConfig(List<SSHConnectionConfig> sshConnections, List<HDFSConnectionConfig> hdfsConnections) {
        this.sshConnections = sshConnections;
        this.hdfsConnections = hdfsConnections;
    }


    public List<HDFSConnectionConfig> getHdfsConnections() {
        return hdfsConnections;
    }

    public void setHdfsConnections(List<HDFSConnectionConfig> hdfsConnections) {
        this.hdfsConnections = hdfsConnections;
    }

    public List<SSHConnectionConfig> getSshConnections() {
        return sshConnections;
    }

    public void setSshConnections(List<SSHConnectionConfig> sshConnections) {
        this.sshConnections = sshConnections;
    }

}
