package org.zcs.desktop.hsshfm.config.connection;

import com.typesafe.config.Optional;

public class HDFSConnectionConfig extends NamedConnection {
    @Optional
    private String configFolder;
    @Optional
    private String keytabFile;
    @Optional
    private String principal;
    @Optional
    private boolean krb5_debug;

    public HDFSConnectionConfig() {
    }

    public HDFSConnectionConfig(String name, String configFolder, String keytabFile, String principal, boolean krb5_debug) {
        this.setName(name);
        this.configFolder = configFolder;
        this.keytabFile = keytabFile;
        this.principal = principal;
        this.krb5_debug = krb5_debug;
    }

    public String getConfigFolder() {
        return configFolder;
    }

    public void setConfigFolder(String configFolder) {
        this.configFolder = configFolder;
    }

    public String getKeytabFile() {
        return keytabFile;
    }

    public void setKeytabFile(String keytabFile) {
        this.keytabFile = keytabFile;
    }

    public String getPrincipal() {
        return principal;
    }

    public void setPrincipal(String principal) {
        this.principal = principal;
    }

    public boolean isKrb5_debug() {
        return krb5_debug;
    }

    public void setKrb5_debug(boolean krb5_debug) {
        this.krb5_debug = krb5_debug;
    }
}
