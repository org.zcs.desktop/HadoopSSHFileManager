package org.zcs.desktop.hsshfm.config;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigBeanFactory;
import com.typesafe.config.ConfigFactory;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ApplicationConfigTest {
    @Test
    public void testWhenConfigExists_ThenFilledSuccessfully() {
        Config config = ConfigFactory.load("org/zcs/desktop/hsshfm/config/ApplicationConfigTest.conf");
        ApplicationConfig actual = ConfigBeanFactory.create(config, ApplicationConfig.class);
        assertEquals(1, actual.getHdfsConnections().size());
        assertEquals(1, actual.getSshConnections().size());
        assertEquals("localhost", actual.getSshConnections().get(0).getHost());
    }
}
