package org.zcs.desktop.hsshfm.config.encryption;

import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;

import static org.junit.Assert.assertEquals;

public class StringDecryptorTest {
    @Rule
    public final EnvironmentVariables environmentVariables = new EnvironmentVariables();

    @Test
    public void testWhenConfigHasEncryptedValue_ThenDecryptedWithoutErrors() {
        environmentVariables.set(StringDecryptor.ENV_VARIABLE_NAME, "jasypt");
        final StringDecryptor testable = new StringDecryptor();
        assertEquals("encryptedValue", testable.decrypt("ENC(c7rI9ySSQg3M9WMp4JE+OLuG0UmmSplg)"));
        assertEquals("plainValue", testable.decrypt("plainValue"));
    }

}
