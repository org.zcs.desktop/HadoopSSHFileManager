package org.zcs.desktop.hsshfm.model;

public class Bookmark {
	private final FileSystemType fileSystemType;
	private final String path;

	public Bookmark(final FileSystemType fileSystemType, final String path) {
		this.fileSystemType = fileSystemType;
		this.path = path;
	}

	public FileSystemType getFileSystemType() {
		return fileSystemType;
	}

	public String getPath() {
		return path;
	}

	@Override
	public String toString() {
		return fileSystemType.toString() + ": " + path.toString();
	}

}
