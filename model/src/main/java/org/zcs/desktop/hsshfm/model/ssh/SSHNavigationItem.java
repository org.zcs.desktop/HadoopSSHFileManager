package org.zcs.desktop.hsshfm.model.ssh;

import org.zcs.desktop.hsshfm.model.NavigationItem;

import com.jcraft.jsch.ChannelSftp.LsEntry;

public class SSHNavigationItem extends NavigationItem {

	private LsEntry entry;
	private String parentDir;

	public LsEntry getEntry() {
		return entry;
	}

	public void setEntry(LsEntry entry) {
		this.entry = entry;
	}

	public String getParentDir() {
		return parentDir;
	}

	public void setParentDir(String parentDir) {
		this.parentDir = parentDir;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((entry == null) ? 0 : entry.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SSHNavigationItem other = (SSHNavigationItem) obj;
		if (entry == null) {
			if (other.entry != null)
				return false;
		} else if (!entry.equals(other.entry))
			return false;
		return true;
	}

}
