package org.zcs.desktop.hsshfm.model;

import java.util.ArrayList;
import java.util.List;

public class NavigationBundle {
    private String path;
    private final List<NavigationItem> itemList = new ArrayList<>();
    private final BundleProvider bundleProvider;

    public NavigationBundle(final BundleProvider bundleProvider) {
        this.bundleProvider = bundleProvider;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public List<NavigationItem> getItemList() {
        return itemList;
    }

    public void addItem(final NavigationItem item) {
        itemList.add(item);
    }

	public BundleProvider getBundleProvider() {
		return bundleProvider;
	}
}
