package org.zcs.desktop.hsshfm.model;

import java.io.IOException;
import java.util.List;

import org.zcs.desktop.hsshfm.model.FileSystemType;
import org.zcs.desktop.hsshfm.model.NavigationBundle;
import org.zcs.desktop.hsshfm.model.NavigationItem;

public interface BundleProvider {
	NavigationBundle getRootNavigationBundle() throws IOException;

	boolean isExpandable(NavigationItem item);

	boolean isExtractable(NavigationItem item);

	NavigationBundle getNavigationBundle(NavigationItem item)
			throws IOException;

	NavigationBundle getNavigationBundle(String path) throws IOException;

	NavigationBundle refresh(String path) throws IOException;

	void close() throws IOException;

	void rm(String path, String name, boolean isFolder) throws IOException;

	void mkdir(String path, String name) throws IOException;

	FileSystemType getFileSystemType();
	
	void extract(NavigationBundle navigationBundle,
			List<NavigationItem> itemList);

}
