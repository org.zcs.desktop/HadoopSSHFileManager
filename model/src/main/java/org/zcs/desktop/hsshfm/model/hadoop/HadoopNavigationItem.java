package org.zcs.desktop.hsshfm.model.hadoop;

import org.zcs.desktop.hsshfm.model.NavigationItem;

public class HadoopNavigationItem extends NavigationItem {

	private String parentDir;

	public String getParentDir() {
		return parentDir;
	}

	public void setParentDir(String parentDir) {
		this.parentDir = parentDir;
	}

}
