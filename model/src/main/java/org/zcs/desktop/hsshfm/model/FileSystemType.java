package org.zcs.desktop.hsshfm.model;

public enum FileSystemType {
	LOCAL, SSH, HADOOP, HADOOP_REMOTE
}
