package org.zcs.desktop.hsshfm.service.ssh;

import com.jcraft.jsch.UIKeyboardInteractive;
import com.jcraft.jsch.UserInfo;

public class SSHUserInfo implements UserInfo,UIKeyboardInteractive {
    private String password;

    public SSHUserInfo(final String password) {
        this.password = password;
    }

    @Override
    public String getPassphrase() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public boolean promptPassword(String message) {
        // TODO Auto-generated method stub
        return true;
    }

    @Override
    public boolean promptPassphrase(String message) {
        return true;
    }

    @Override
    public boolean promptYesNo(String message) {
        return false;
    }

    @Override
    public void showMessage(String message) {
        System.out.println("message: "+message);
    }

    @Override
    public String[] promptKeyboardInteractive(String destination, String name, String instruction, String[] prompt, boolean[] echo) {
        // TODO Auto-generated method stub
        return new String[] {password};
    }

}
