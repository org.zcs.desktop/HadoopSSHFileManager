package org.zcs.desktop.hsshfm.service.fileoperation.copy;

import java.io.IOException;
import java.util.List;

import org.apache.log4j.Logger;
import org.zcs.desktop.hsshfm.model.NavigationBundle;
import org.zcs.desktop.hsshfm.model.NavigationItem;
import org.zcs.desktop.hsshfm.service.BundleProviderFactory;
import org.zcs.desktop.hsshfm.service.fileoperation.FileOperation;

public class CopySSHHadoopOperation implements FileOperation {
	private final static Logger LOGGER = Logger
			.getLogger(CopySSHHadoopOperation.class);

	private final List<NavigationItem> itemList;
	private final NavigationBundle destinationBundle;
	private final NavigationBundle sourceBundle;

	private BundleProviderFactory bundleFactory;

	public CopySSHHadoopOperation(NavigationBundle sourceBundle,
			List<NavigationItem> itemList,
			final NavigationBundle destinationBundle) {
		this.sourceBundle = sourceBundle;
		this.itemList = itemList;
		this.destinationBundle = destinationBundle;
	}

	@Override
	public void progress() throws IOException {
		System.out.println("Copy from SSH to Hadoop directory");
		// FIXME
//		final HadoopBundleProvider hadoopProvider = (HadoopBundleProvider) bundleFactory
//				.getProvider(FileSystemType.HADOOP);
//		try {
//			for (NavigationItem current : itemList) {
//				final String sshName = sourceBundle.getPath() + "/"
//						+ current.getName();
//				LOGGER.info("copy from " + sshName + " to "
//						+ destinationBundle.getPath());
//
//				System.out.println("PUT: " + sshName + ":  "
//						+ destinationBundle.getPath());
//				hadoopProvider.put(sshName, destinationBundle.getPath());
//			}
//		} catch (Exception e) {
//			throw new IOException(e);
//		}
	}

	public void setBundleFactory(BundleProviderFactory bundleFactory) {
		this.bundleFactory = bundleFactory;
	}
}
