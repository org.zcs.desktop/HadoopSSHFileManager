package org.zcs.desktop.hsshfm.service.ssh;

import java.util.Comparator;

import org.zcs.desktop.hsshfm.model.NavigationItem;

public class NavigationItemComparator implements Comparator<NavigationItem> {
	@Override
	public int compare(NavigationItem f1, NavigationItem f2) {
		if (f1.isFolder()) {
			if (!f2.isFolder()) {
				return -1;
			}
			return f1.getName().compareToIgnoreCase(f2.getName());
		}
		if (f2.isFolder()) {
			return 1;
		}
		return f1.getName().compareToIgnoreCase(f2.getName());
	}
}