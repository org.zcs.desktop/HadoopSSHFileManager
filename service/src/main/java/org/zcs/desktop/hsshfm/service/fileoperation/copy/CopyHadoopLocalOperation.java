package org.zcs.desktop.hsshfm.service.fileoperation.copy;

import org.zcs.desktop.hsshfm.model.NavigationBundle;
import org.zcs.desktop.hsshfm.model.NavigationItem;
import org.zcs.desktop.hsshfm.service.fileoperation.FileOperation;
import org.zcs.desktop.hsshfm.service.hadoop.HadoopBundleProvider;

import java.io.IOException;
import java.util.List;

public class CopyHadoopLocalOperation implements FileOperation {
	private final NavigationBundle sourceBundle;
	private final List<NavigationItem> itemList;
	private final NavigationBundle destinationBundle;

	public CopyHadoopLocalOperation(NavigationBundle sourceBundle,
			List<NavigationItem> itemList,
			final NavigationBundle destinationBundle) {
		this.sourceBundle = sourceBundle;
		this.itemList = itemList;
		this.destinationBundle = destinationBundle;
	}

	@Override
	public void progress() throws IOException {
		final HadoopBundleProvider hadoopProvider = (HadoopBundleProvider) sourceBundle.getBundleProvider();
		for (NavigationItem current : itemList) {
			hadoopProvider.copyToLocalFile(sourceBundle.getPath(), current.getName(), destinationBundle.getPath());
		}
	}
}
