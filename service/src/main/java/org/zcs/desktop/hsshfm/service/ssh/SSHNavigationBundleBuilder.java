package org.zcs.desktop.hsshfm.service.ssh;

import java.util.Collections;
import java.util.Date;
import java.util.Vector;

import org.zcs.desktop.hsshfm.model.NavigationBundle;
import org.zcs.desktop.hsshfm.model.NavigationItem;
import org.zcs.desktop.hsshfm.model.ssh.SSHNavigationItem;

import com.jcraft.jsch.ChannelSftp.LsEntry;

public class SSHNavigationBundleBuilder {

	public NavigationBundle build(SSHBundleProvider sshBundleProvider, final String path, Vector<LsEntry> entires) {
		final NavigationBundle result = new NavigationBundle(sshBundleProvider);
		result.setPath(path);
		for (LsEntry current : entires) {
			if (!".".equals(current.getFilename())) {
				result.addItem(createNavigationItem(current, path));
			}
		}
		Collections.sort(result.getItemList(),
				new NavigationItemComparator());
		return result;
	}

	private NavigationItem createNavigationItem(final LsEntry entry,
                                                final String parentDir) {
		final SSHNavigationItem result = new SSHNavigationItem();
		result.setEntry(entry);
		result.setName(entry.getFilename());
		result.setFolder(entry.getAttrs().isDir());
		if ("..".equals(entry.getFilename())) {
			result.setParentDir(getParentDir(parentDir));
		} else {
			result.setParentDir(parentDir);
		}

		result.setChanged(new Date(entry.getAttrs().getATime()));
		result.setSize(entry.getAttrs().getSize());
		return result;
	}

	private String getParentDir(final String path) {
		if (path.equals(".")) {
			return path;
		}
		final String[] parts = path.split("/");
		StringBuilder result = new StringBuilder();
		for (int i = 0; i < parts.length - 1; i++) {
			result.append(parts[i]);
			result.append("/");
		}
		result.deleteCharAt(result.length() - 1);
		return result.toString();
	}

}
