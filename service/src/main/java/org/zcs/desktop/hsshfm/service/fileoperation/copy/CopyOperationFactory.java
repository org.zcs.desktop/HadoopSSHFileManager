package org.zcs.desktop.hsshfm.service.fileoperation.copy;

import java.util.List;

import org.zcs.desktop.hsshfm.model.NavigationBundle;
import org.zcs.desktop.hsshfm.model.NavigationItem;
import org.zcs.desktop.hsshfm.service.BundleProviderFactory;
import org.zcs.desktop.hsshfm.service.fileoperation.FileOperation;

public class CopyOperationFactory {
	private BundleProviderFactory bundleFactory;

	public FileOperation getCopyOperation(NavigationBundle sourceBundle,
                                          List<NavigationItem> itemList,
                                          final NavigationBundle destinationBundle) {
		switch (sourceBundle.getBundleProvider().getFileSystemType()) {
		case LOCAL:
			return getLocalSourceOperation(sourceBundle, itemList,
					destinationBundle);
		case HADOOP:
			return getHadoopSourceOperation(sourceBundle, itemList,
					destinationBundle);
		case SSH:
			return getSSHSourceOperation(sourceBundle, itemList,
					destinationBundle);
		}
		return null;
	}

	private FileOperation getLocalSourceOperation(
			NavigationBundle sourceBundle, List<NavigationItem> itemList,
			final NavigationBundle destinationBundle) {
		switch (destinationBundle.getBundleProvider().getFileSystemType()) {
		case LOCAL:
//			return new CopyLocalLocalOperation(sourceBundle, itemList,
//					destinationBundle);
		case HADOOP:
			return new CopyLocalHadoopOperation(sourceBundle, itemList, destinationBundle);
		case SSH:
//			final CopyLocalSSHOperation operation = new CopyLocalSSHOperation(
//					sourceBundle, itemList, destinationBundle);
//			operation.setBundleFactory(bundleFactory);
//			return operation;
		}
		
		return null;
	}

	FileOperation getHadoopSourceOperation(NavigationBundle sourceBundle,
			List<NavigationItem> itemList,
			final NavigationBundle destinationBundle) {
		switch (destinationBundle.getBundleProvider().getFileSystemType()) {
		case LOCAL:
			return new CopyHadoopLocalOperation(
					sourceBundle, itemList, destinationBundle);
		}
		return null;
	}
	
	FileOperation getSSHSourceOperation(NavigationBundle sourceBundle,
			List<NavigationItem> itemList,
			final NavigationBundle destinationBundle) {
		// FIXME
//		switch (destinationBundle.getType()) {
//		case HADOOP:
//			final CopySSHHadoopOperation result = new CopySSHHadoopOperation(
//					sourceBundle, itemList, destinationBundle);
//			result.setBundleFactory(bundleFactory);
//			return result;
//		case LOCAL:
//			final CopySSHLocalOperation operation = new CopySSHLocalOperation(
//					sourceBundle, itemList, destinationBundle);
//			operation.setBundleFactory(bundleFactory);
//			return operation;
//		}
		return null;
	}
	

	public void setBundleFactory(BundleProviderFactory bundleFactory) {
		this.bundleFactory = bundleFactory;
	}

}
