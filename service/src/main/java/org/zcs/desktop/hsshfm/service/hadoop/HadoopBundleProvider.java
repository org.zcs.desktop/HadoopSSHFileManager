package org.zcs.desktop.hsshfm.service.hadoop;

import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zcs.desktop.hsshfm.config.connection.HDFSConnectionConfig;
import org.zcs.desktop.hsshfm.model.BundleProvider;
import org.zcs.desktop.hsshfm.model.FileSystemType;
import org.zcs.desktop.hsshfm.model.NavigationBundle;
import org.zcs.desktop.hsshfm.model.NavigationItem;
import org.zcs.desktop.hsshfm.model.hadoop.HadoopNavigationItem;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class HadoopBundleProvider implements BundleProvider {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final FileSystemProvider fileSystemProvider = new FileSystemProvider();
    private final FileSystem fileSystem;

    public HadoopBundleProvider(HDFSConnectionConfig hdfsConnectionConfig) throws IOException {
        this.fileSystem = fileSystemProvider.get(hdfsConnectionConfig);
    }

    @Override
    public NavigationBundle getRootNavigationBundle() throws IOException {
        return ls("/");
    }

    @Override
    public boolean isExpandable(NavigationItem item) {
        return item.isFolder();
    }

    @Override
    public boolean isExtractable(NavigationItem item) {
        return false;
    }

    @Override
    public NavigationBundle getNavigationBundle(NavigationItem item) throws IOException {
        final HadoopNavigationItem local = (HadoopNavigationItem) item;
        final String path;
        if ("..".equals(item.getName())) {
            path = local.getParentDir();
        } else {
            path = local.getParentDir() + local.getName() + "/";
        }
        return ls(path);
    }

    @Override
    public NavigationBundle getNavigationBundle(String path) throws IOException {
        return ls(path);
    }

    @Override
    public NavigationBundle refresh(String path) throws IOException {
        return ls(path);
    }

    @Override
    public void close() throws IOException {
        fileSystem.close();
    }

    @Override
    public void rm(String path, String name, boolean isFolder) throws IOException {
        final Path dirPath = new Path(path, name);
        logger.info("Removing dir "+dirPath.toString());
        fileSystem.delete(dirPath, true);
    }

    @Override
    public void mkdir(String path, String name) throws IOException {
        final Path dirPath = new Path(path, name);
        logger.info("Creating dir {}", dirPath.toString());
        fileSystem.mkdirs(dirPath);
    }

    @Override
    public FileSystemType getFileSystemType() {
        return FileSystemType.HADOOP;
    }

    @Override
    public void extract(NavigationBundle navigationBundle, List<NavigationItem> itemList) {

    }
    public void copyToLocalFile(String sourceFolder, String sourceFileName, String destinationFolder) throws IOException {
        final Path sourcePath = new Path(sourceFolder, sourceFileName);
        logger.info("Copy remote file {} to {}", sourcePath.toString(), destinationFolder);
        fileSystem.copyToLocalFile(sourcePath, new Path(destinationFolder, sourceFileName));
    }

    public void copyFromLocalFile(String sourceFolder, String sourceFileName, String destinationFolder) throws IOException {
        final Path sourcePath = new Path(sourceFolder, sourceFileName);
        logger.info("Copy from local file {} to {}", sourcePath.toString(), destinationFolder);
        fileSystem.copyFromLocalFile(sourcePath, new Path(destinationFolder, sourceFileName));
    }

    private NavigationBundle ls(String path) throws IOException {
        List<HadoopNavigationItem> itemList = Arrays.stream(fileSystem.listStatus(new Path(path))).map(fs -> createNavigationItem(fs, path)).collect(Collectors.toList());
        if (!"/".equals(path)) {
            itemList.add(createRootNavigationItem(path));
        }
        Collections.sort(itemList, new NavigationItemNameComparator());
        final NavigationBundle result = new NavigationBundle(this);
        result.setPath(path);
        for (HadoopNavigationItem current : itemList) {
            result.addItem(current);
        }
        return result;

    }

    protected HadoopNavigationItem createNavigationItem(final FileStatus fs,
                                                        final String parentDir) {
        final HadoopNavigationItem result = new HadoopNavigationItem();
        result.setSize(fs.getLen());
        result.setName(fs.getPath().getName());
        result.setParentDir(parentDir);
        result.setFolder(fs.isDirectory());
        result.setChanged(new Date(fs.getModificationTime()));
        return result;
    }

    private HadoopNavigationItem createRootNavigationItem(final String path) {
        final HadoopNavigationItem result = new HadoopNavigationItem();
        result.setName("..");
        result.setParentDir(getParentDir(path));
        result.setFolder(true);
        return result;
    }

    private String getParentDir(final String path) {
        final String[] parts = path.split("/");
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < parts.length - 1; i++) {
            result.append(parts[i]);
            result.append("/");
        }
        return result.toString();
    }
}
