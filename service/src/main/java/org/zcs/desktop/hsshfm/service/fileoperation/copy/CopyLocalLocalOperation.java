package org.zcs.desktop.hsshfm.service.fileoperation.copy;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.zcs.desktop.hsshfm.model.NavigationBundle;
import org.zcs.desktop.hsshfm.model.NavigationItem;
import org.zcs.desktop.hsshfm.model.local.LocalNavigationItem;
import org.zcs.desktop.hsshfm.service.fileoperation.FileOperation;

public class CopyLocalLocalOperation implements FileOperation {
	private final NavigationBundle sourceBundle;
	private final List<NavigationItem> itemList;
	private final NavigationBundle destinationBundle;

	public CopyLocalLocalOperation(NavigationBundle sourceBundle,
			List<NavigationItem> itemList,
			final NavigationBundle destinationBundle) {
		this.sourceBundle = sourceBundle;
		this.itemList = itemList;
		this.destinationBundle = destinationBundle;
	}
	@Override
	public void progress() throws IOException {
		System.out.println("source root: " + sourceBundle.getPath());
		System.out.println("destinatrion root: " + destinationBundle.getPath());
		final File destinationFolder = new File(destinationBundle.getPath());
		for (NavigationItem current : itemList) {
			final LocalNavigationItem local = (LocalNavigationItem) current;
			if (local.getFile().isDirectory()) {
				FileUtils.copyDirectoryToDirectory(local.getFile(),
						destinationFolder);
			} else {
				FileUtils.copyFileToDirectory(local.getFile(),
						destinationFolder);
			}
		}

	}

}
