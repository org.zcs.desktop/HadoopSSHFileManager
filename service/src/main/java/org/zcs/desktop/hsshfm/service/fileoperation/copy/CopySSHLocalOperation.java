package org.zcs.desktop.hsshfm.service.fileoperation.copy;

import java.io.IOException;
import java.util.List;

import org.apache.log4j.Logger;
import org.zcs.desktop.hsshfm.model.NavigationBundle;
import org.zcs.desktop.hsshfm.model.NavigationItem;
import org.zcs.desktop.hsshfm.service.BundleProviderFactory;
import org.zcs.desktop.hsshfm.service.fileoperation.FileOperation;

public class CopySSHLocalOperation implements FileOperation {
	private final static Logger LOGGER = Logger
			.getLogger(CopySSHLocalOperation.class);

	private final List<NavigationItem> itemList;
	private final NavigationBundle destinationBundle;
	private final NavigationBundle sourceBundle;

	private BundleProviderFactory bundleFactory;

	public CopySSHLocalOperation(NavigationBundle sourceBundle,
			List<NavigationItem> itemList,
			final NavigationBundle destinationBundle) {
		this.sourceBundle = sourceBundle;
		this.itemList = itemList;
		this.destinationBundle = destinationBundle;
	}

	@Override
	public void progress() throws IOException {
		// FIXME
		
//		final SSHBundleProvider sshProvider = (SSHBundleProvider) bundleFactory
//				.getProvider(FileSystemType.SSH);
//		try {
//			for (NavigationItem current : itemList) {
//				final String sourcePath = sourceBundle.getPath() + "/"
//						+ current.getName();
//				LOGGER.info("Copy from SSH "+sourcePath+"to local"+destinationBundle.getPath());
//				sshProvider
//						.get(sourcePath, new File(destinationBundle.getPath(),
//								current.getName()).getPath());
//			}
//		} catch (Exception e) {
//			throw new IOException(e);
//		}
	}

	public void setBundleFactory(BundleProviderFactory bundleFactory) {
		this.bundleFactory = bundleFactory;
	}
}
