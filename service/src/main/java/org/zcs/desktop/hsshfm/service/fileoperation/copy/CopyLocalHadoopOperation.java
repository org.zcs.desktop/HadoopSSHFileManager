package org.zcs.desktop.hsshfm.service.fileoperation.copy;

import org.zcs.desktop.hsshfm.model.NavigationBundle;
import org.zcs.desktop.hsshfm.model.NavigationItem;
import org.zcs.desktop.hsshfm.service.fileoperation.FileOperation;
import org.zcs.desktop.hsshfm.service.hadoop.HadoopBundleProvider;

import java.io.IOException;
import java.util.List;

public class CopyLocalHadoopOperation implements FileOperation {
    private final NavigationBundle sourceBundle;
    private final List<NavigationItem> itemList;
    private final NavigationBundle destinationBundle;

    public CopyLocalHadoopOperation(NavigationBundle sourceBundle,
                                    List<NavigationItem> itemList,
                                    final NavigationBundle destinationBundle) {
        this.sourceBundle = sourceBundle;
        this.itemList = itemList;
        this.destinationBundle = destinationBundle;
    }

    @Override
    public void progress() throws IOException {
        final HadoopBundleProvider hadoopProvider = (HadoopBundleProvider) destinationBundle.getBundleProvider();
        for (NavigationItem current : itemList) {
            hadoopProvider.copyFromLocalFile(sourceBundle.getPath(), current.getName(), destinationBundle.getPath());
        }
    }

}
