package org.zcs.desktop.hsshfm.service.hadoop;

import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.security.UserGroupInformation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zcs.desktop.hsshfm.config.connection.HDFSConnectionConfig;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FileSystemProvider {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    public static String KERBEROS_CONFIG_FILE_NAME = "krb5.conf";
    public static String KERBEROS_FILE_PROPERTY = "java.security.krb5.conf";

    public FileSystem get(final HDFSConnectionConfig hdfsConnectionConfig) throws IOException {
        logger.info("Getting connection for config {}", hdfsConnectionConfig.getName());
        final Configuration conf = createConfiguration(hdfsConnectionConfig);
        if (hdfsConnectionConfig.isKrb5_debug()) {
            System.setProperty("sun.security.krb5.debug", "true");
        }
        loginWithKeytab(conf, hdfsConnectionConfig.getKeytabFile(), hdfsConnectionConfig.getPrincipal());
        return FileSystem.get(conf);
    }

    private Configuration createConfiguration(final HDFSConnectionConfig hdfsConnectionConfig) throws IOException {
        final Configuration conf = new Configuration();
        if (StringUtils.isNotEmpty(hdfsConnectionConfig.getConfigFolder())) {
            final java.nio.file.Path configDirPath = Paths.get(hdfsConnectionConfig.getConfigFolder());
            if (Files.exists(configDirPath)) {
                Files
                        .list(configDirPath)
                        .filter(f -> f.getFileName().toFile().getName().endsWith(".xml"))
                        .forEach(f -> conf.addResource(new Path(f.toFile().getPath())));
            }
            final java.nio.file.Path kerberosFilePath = configDirPath.resolve(KERBEROS_CONFIG_FILE_NAME);
            if (Files.exists(kerberosFilePath)) {
                logger.info("Set kerberos file: {}", kerberosFilePath.toFile().getPath());
                System.setProperty(KERBEROS_FILE_PROPERTY, kerberosFilePath.toFile().getPath());
            }
        }
        return conf;
    }

    private void loginWithKeytab(final Configuration conf, final String keytabFile, final String principal) throws IOException {
        if (StringUtils.isNotEmpty(keytabFile) && StringUtils.isNotEmpty(principal)) {
            logger.info("Logging with Kerberos keytab {}", keytabFile);
            UserGroupInformation.setConfiguration(conf);
            UserGroupInformation.loginUserFromKeytab(principal, keytabFile);
        }
    }
}
