package org.zcs.desktop.hsshfm.service.fileoperation.copy;

import java.io.IOException;
import java.util.List;

import org.apache.log4j.Logger;
import org.zcs.desktop.hsshfm.model.NavigationBundle;
import org.zcs.desktop.hsshfm.model.NavigationItem;
import org.zcs.desktop.hsshfm.service.BundleProviderFactory;
import org.zcs.desktop.hsshfm.service.fileoperation.FileOperation;

public class CopyLocalSSHOperation implements FileOperation {
	private final static Logger LOGGER = Logger
			.getLogger(CopyLocalSSHOperation.class);

	private final List<NavigationItem> itemList;
	private final NavigationBundle destinationBundle;

	private BundleProviderFactory bundleFactory;

	public CopyLocalSSHOperation(NavigationBundle sourceBundle,
			List<NavigationItem> itemList,
			final NavigationBundle destinationBundle) {
		this.itemList = itemList;
		this.destinationBundle = destinationBundle;
	}

	@Override
	public void progress() throws IOException {
		// FIXME
//		final SSHBundleProvider sshProvider = (SSHBundleProvider) bundleFactory
//				.getProvider(FileSystemType.SSH);
//		try {
//			for (NavigationItem current : itemList) {
//				final LocalNavigationItem localItem = (LocalNavigationItem) current;
//				final String destinationPath = destinationBundle.getPath()
//						+ "/" + localItem.getName();
//				LOGGER.info("copy from " + localItem.getFile().getPath()
//						+ " to " + destinationPath);
//				sshProvider.put(localItem.getFile().getPath(), destinationPath,
//						current.isFolder());
//				LOGGER.info("finished");
//			}
//		} catch (Exception e) {
//			throw new IOException(e);
//		}
	}

	public void setBundleFactory(BundleProviderFactory bundleFactory) {
		this.bundleFactory = bundleFactory;
	}
}
