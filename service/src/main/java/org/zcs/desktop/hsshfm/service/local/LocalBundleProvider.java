package org.zcs.desktop.hsshfm.service.local;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.zcs.desktop.hsshfm.model.FileSystemType;
import org.zcs.desktop.hsshfm.model.NavigationBundle;
import org.zcs.desktop.hsshfm.model.NavigationItem;
import org.zcs.desktop.hsshfm.model.local.LocalNavigationItem;
import org.zcs.desktop.hsshfm.model.BundleProvider;

public class LocalBundleProvider implements BundleProvider {
	private final static Logger LOGGER = Logger
			.getLogger(LocalBundleProvider.class);

	@Override
	public NavigationBundle getRootNavigationBundle() {
		final String path = System.getProperty("user.dir");
		final File initDir = new File(path);
		return getNavigationBundle(initDir);
	}

	@Override
	public boolean isExpandable(final NavigationItem item) {
		final LocalNavigationItem local = (LocalNavigationItem) item;
		return local.getFile().isDirectory();
	}

	@Override
	public NavigationBundle getNavigationBundle(final NavigationItem item) {
		final LocalNavigationItem local = (LocalNavigationItem) item;
		return getNavigationBundle(local.getFile());
	}

	private NavigationBundle getNavigationBundle(final File file) {
		final NavigationBundle result = new NavigationBundle(this);
		result.setPath(file.getPath());
		final File parentFile = file.getParentFile();
		if (file.getParent() != null) {
			result.addItem(createRootNavigationItem(parentFile));
		}

		File[] files = file.listFiles();
		Arrays.sort(files, new FileNameComparator());
		for (File current : files) {
			result.addItem(createNavigationItem(current));
		}
		return result;
	}

	private NavigationItem createNavigationItem(final File file) {
		final LocalNavigationItem result = new LocalNavigationItem();
		result.setName(file.getName());
		result.setFile(file);
		result.setChanged(new Date(file.lastModified()));

		result.setFolder(file.isDirectory());
		if (!file.isDirectory()) {
			result.setSize(file.length());
		}
		return result;
	}

	private NavigationItem createRootNavigationItem(final File file) {
		final LocalNavigationItem result = new LocalNavigationItem();
		result.setFile(file);
		result.setName("..");
		result.setFolder(true);
		return result;
	}

	public static class FileNameComparator implements Comparator<File> {
		@Override
		public int compare(File f1, File f2) {
			if (f1.isDirectory()) {
				if (!f2.isDirectory()) {
					return -1;
				}
				return f1.getName().compareToIgnoreCase(f2.getName());
			}
			if (f2.isDirectory()) {
				return 1;
			}
			return f1.getName().compareToIgnoreCase(f2.getName());
		}
	}

	@Override
	public void close() throws IOException {
	}

	@Override
	public NavigationBundle refresh(String path) throws IOException {
		LOGGER.info("refresh: " + path);
		return getNavigationBundle(new File(path));
	}

	@Override
	public void rm(String path, String name, boolean isFolder) {
		LOGGER.info("delete file: " + path + ", " + name);
		final File file = new File(path, name);
		try {
			if (file.exists()) {
				if (isFolder) {
					FileUtils.deleteDirectory(file);
				} else {
					file.delete();
				}
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void mkdir(String path, String name) {
		LOGGER.info("creating dir: " + path + "," + name);
		new File(path, name).mkdir();
	}

	@Override
	public FileSystemType getFileSystemType() {
		return FileSystemType.LOCAL;
	}

	@Override
	public NavigationBundle getNavigationBundle(String path) throws IOException {
		return getNavigationBundle(new File(path));
	}

	@Override
	public boolean isExtractable(NavigationItem item) {
		return false;
	}

	@Override
	public void extract(NavigationBundle navigationBundle,
			List<NavigationItem> itemList) {
		// TODO Auto-generated method stub
		
	}

}
