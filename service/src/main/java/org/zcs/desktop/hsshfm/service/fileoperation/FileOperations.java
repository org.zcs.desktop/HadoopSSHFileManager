package org.zcs.desktop.hsshfm.service.fileoperation;

import org.zcs.desktop.hsshfm.model.NavigationBundle;
import org.zcs.desktop.hsshfm.model.NavigationItem;
import org.zcs.desktop.hsshfm.model.BundleProvider;
import org.zcs.desktop.hsshfm.service.BundleProviderFactory;
import org.zcs.desktop.hsshfm.service.fileoperation.copy.CopyOperationFactory;

import java.io.IOException;
import java.util.List;

public class FileOperations {
	private BundleProviderFactory bundleFactory;
	private CopyOperationFactory copyOperationFactory = new CopyOperationFactory();

	public void copy(NavigationBundle sourceBundle,
			List<NavigationItem> itemList,
			final NavigationBundle destinationBundle) throws IOException {
		final FileOperation copyOperation = copyOperationFactory
				.getCopyOperation(sourceBundle, itemList, destinationBundle);
		copyOperation.progress();
	}

	public void rm(NavigationBundle sourceBundle, List<NavigationItem> itemList)
			throws IOException {
		final BundleProvider provider = sourceBundle.getBundleProvider();
		for (NavigationItem current : itemList) {
			provider.rm(sourceBundle.getPath(), current.getName(),
					current.isFolder());
		}
	}

	public void mkdir(NavigationBundle sourceBundle, final String name)
			throws IOException {
		sourceBundle.getBundleProvider().mkdir(sourceBundle.getPath(), name);
	}

	public void setBundleFactory(BundleProviderFactory bundleFactory) {
		this.bundleFactory = bundleFactory;
		copyOperationFactory.setBundleFactory(bundleFactory);
	}
}
