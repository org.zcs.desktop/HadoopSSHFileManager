package org.zcs.desktop.hsshfm.service.ssh;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Vector;

import com.jcraft.jsch.*;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zcs.desktop.hsshfm.config.connection.SSHConnectionConfig;
import org.zcs.desktop.hsshfm.model.FileSystemType;
import org.zcs.desktop.hsshfm.model.NavigationBundle;
import org.zcs.desktop.hsshfm.model.NavigationItem;
import org.zcs.desktop.hsshfm.model.ssh.SSHNavigationItem;
import org.zcs.desktop.hsshfm.model.BundleProvider;

import com.jcraft.jsch.ChannelSftp.LsEntry;

public class SSHBundleProvider implements BundleProvider {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final SSHNavigationBundleBuilder bundleBuilder = new SSHNavigationBundleBuilder();
    private ChannelSftp channel;
    private final Session session;

    public SSHBundleProvider(SSHConnectionConfig sshConnectionConfig) throws IOException {
        try {
            session = new SSHUtils(sshConnectionConfig).createSession();
        } catch (JSchException e) {
            throw new IOException(e);
        }
    }

    @Override
    public NavigationBundle getRootNavigationBundle() {
        try {
            return getNavigationBundle(".");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean isExpandable(final NavigationItem item) {
        return item.isFolder();
    }

    @Override
    public NavigationBundle getNavigationBundle(final NavigationItem item) {
        final SSHNavigationItem local = (SSHNavigationItem) item;
        try {
            return getNavigationBundle(getPath(local));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private String getPath(final SSHNavigationItem local) {
        final String result;
        if ("..".equals(local.getName())) {
            System.out.println("get parent dir: " + local.getParentDir());
            result = local.getParentDir();
        } else {
            result = local.getParentDir() + "/" + local.getName();
        }
        return result;
    }

    public void close() {
        if (channel != null) {
            channel.disconnect();
        }
        if (session != null) {
            session.disconnect();
        }
    }

    public NavigationBundle getNavigationBundle(final String path)
            throws IOException {
        try {
            return bundleBuilder.build(this, path, ls(path));
        } catch (Exception e) {
            throw new IOException(e);
        }

    }

    @SuppressWarnings("unchecked")
    public Vector<LsEntry> ls(final String path) throws SftpException,
            JSchException {
        System.out.println("ls: " + path);
        return getChannel().ls(path);
    }

    public void get(final String remotePath, final String localPath)
            throws SftpException, JSchException {
        getChannel().get(remotePath, localPath, new SftpProgressMonitorImpl(),
                ChannelSftp.OVERWRITE);
    }

    public void put(final String src, final String dst, final boolean isFolder)
            throws SftpException, JSchException {
        logger.info("Put from " + src + " to " + dst);
        if (isFolder) {
            final File srcFile = new File(src);
            copyDirectory(srcFile, dst);
            System.out.println(srcFile.exists());
        } else {
            getChannel().put(src, dst, ChannelSftp.OVERWRITE);
        }
    }

    private void copyDirectory(final File srcFile, final String dst)
            throws SftpException, JSchException {
        getChannel().mkdir(dst);
        for (final File current : srcFile.listFiles()) {
            if (current.isDirectory()) {
                copyDirectory(current, dst + "/" + current.getName());
            } else {
                getChannel().put(current.getPath(),
                        dst + "/" + current.getName(), ChannelSftp.OVERWRITE);
            }
        }
    }

    public void rm(final String remotePath, final boolean isDirectory)
            throws SftpException, JSchException {
        logger.info("Delete " + remotePath + ", isFolder: " + isDirectory);
        if (isDirectory) {
            rmDir(remotePath);
        } else {
            getChannel().rm(remotePath);
        }
    }

    @Override
    public NavigationBundle refresh(String path) throws IOException {
        try {
            return getNavigationBundle(path);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public void rm(String path, String name, boolean isFolder) {
        try {
            rm(path + "/" + name, isFolder);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private void rmDir(final String filePath) {
        final String command = "rm -r " + filePath;
        ChannelExec channel = null;
        try {
            channel = (ChannelExec) session.openChannel("exec");
            channel.setCommand(command);
            channel.connect();
            logger.info(IOUtils.toString(channel.getInputStream()));
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            if (channel != null) {
                channel.disconnect();
            }
        }
    }

    @Override
    public void mkdir(String path, String name) {
        logger.info("creating dir: " + path + "," + name);
        try {
            getChannel().mkdir(path + "/" + name);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public FileSystemType getFileSystemType() {
        return FileSystemType.SSH;
    }

    @Override
    public boolean isExtractable(NavigationItem item) {
        return item.getName().endsWith(".tar.gz");
    }

    private ChannelSftp getChannel() throws JSchException {
        if (this.channel == null) {
            channel = (ChannelSftp) session.openChannel("sftp");
            channel.connect();
        }
        return channel;
    }

    class SftpProgressMonitorImpl implements SftpProgressMonitor {

        @Override
        public void init(int op, String src, String dest, long max) {
            System.out.println("ssh init: " + op + ", " + src + ", " + dest
                    + ", " + max);

        }

        @Override
        public boolean count(long count) {
            System.out.println("ssh count: " + count);
            return true;
        }

        @Override
        public void end() {

        }

    }

    @Override
    public void extract(NavigationBundle navigationBundle,
                        List<NavigationItem> itemList) {
        for (final NavigationItem current : itemList) {
            if (isExtractable(current)) {
                extractItem(navigationBundle, current);
            }
        }
    }

    private void extractItem(final NavigationBundle navigationBundle,
                             final NavigationItem current) {
        final String command = "cd " + navigationBundle.getPath()
                + ";tar xzvf " + current.getName();
        ChannelExec channel = null;
        try {
            channel = (ChannelExec) session.openChannel("exec");
            channel.setCommand(command);
            channel.connect();
            logger.info(IOUtils.toString(channel.getInputStream()));
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            if (channel != null) {
                channel.disconnect();
            }
        }
    }

}
