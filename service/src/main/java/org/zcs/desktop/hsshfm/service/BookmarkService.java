package org.zcs.desktop.hsshfm.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.zcs.desktop.hsshfm.model.Bookmark;
import org.zcs.desktop.hsshfm.model.FileSystemType;

public class BookmarkService {
	private static final String BOOKMARK_FILE_NAME = "bookmarks.txt";
	private static final String SEPARATOR = ";";

	public List<Bookmark> list() throws IOException {
		final File file = new File(BOOKMARK_FILE_NAME);
		final List<Bookmark> result;
		if (file.exists()) {
			result = createBookmarksFromFile(file);
		} else {
			result = Collections.emptyList();
		}
		return result;
	}

	private List<Bookmark> createBookmarksFromFile(final File file)
			throws IOException {
		final List<Bookmark> result = new ArrayList<Bookmark>();
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new InputStreamReader(
					new FileInputStream(BOOKMARK_FILE_NAME)));
			for (String current = reader.readLine(); current != null; current = reader
					.readLine()) {
				final int index = current.indexOf(SEPARATOR);
				if (index == -1) {
					throw new RuntimeException(String.format(
							"Format file %s is not valid!", BOOKMARK_FILE_NAME));
				}
				final FileSystemType type = FileSystemType.valueOf(current
						.substring(0, index));
				final String path = current.substring(index+1);
				result.add(new Bookmark(type, path));
			}
		} finally {
			if (reader != null) {
				reader.close();
			}
		}
		return result;
	}

	public void add(final Bookmark bookmark) throws IOException {
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(BOOKMARK_FILE_NAME, true);
			final String row = bookmark.getFileSystemType().name() + SEPARATOR
					+ bookmark.getPath() + System.getProperty("line.separator");
			fos.write(row.getBytes());
		} finally {
			fos.close();
		}
	}

}
