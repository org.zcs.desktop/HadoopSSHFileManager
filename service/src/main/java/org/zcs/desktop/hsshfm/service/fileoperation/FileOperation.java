package org.zcs.desktop.hsshfm.service.fileoperation;

import java.io.IOException;

public interface FileOperation {
	void progress() throws IOException;
}