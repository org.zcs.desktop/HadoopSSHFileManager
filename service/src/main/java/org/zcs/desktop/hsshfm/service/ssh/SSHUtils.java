package org.zcs.desktop.hsshfm.service.ssh;

import java.io.FileInputStream;
import java.util.Objects;
import java.util.Properties;

import com.jcraft.jsch.*;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.zcs.desktop.hsshfm.config.connection.SSHConnectionConfig;

public class SSHUtils {
    protected final Logger logger = Logger.getLogger(SSHUtils.class);
    private final SSHConnectionConfig config;

    private final static JSch jsch = new JSch();

    public SSHUtils(final SSHConnectionConfig config) {
        this.config = config;
    }

    public Session createSession() throws JSchException {
//        if (Objects.nonNull(config.getPkeyPath())) {
//            return createByPrivateKey();
//        }
        return createByUserNamePassword();
    }

    //    private static Session createByPrivateKey() throws JSchException {
//        jsch.addIdentity(get(PRVKEY));
//        System.out.println(get(PRVKEY) + "," + get(USERNAME) + "," + get(HOST));
//        final Session session = jsch.getSession(get(USERNAME), get(HOST), Integer.parseInt(get(PORT)));
//
//        final Properties config = new Properties();
//        config.put("StrictHostKeyChecking", "no");
//        setProxy(session);
//        session.setConfig(config);
//        session.connect();
//        return session;
//    }
//
    private Session createByUserNamePassword() throws JSchException {
        logger.info(String.format("Host: %s, port: %s, user: %s", config.getHost(), config.getPort(), config.getUser()));
        final Session session = jsch.getSession(config.getUser(), config.getHost(), config.getPort());
        final UserInfo ui = new SSHUserInfo(config.getPassword());
        final Properties config = new Properties();
        config.put("StrictHostKeyChecking", "no");
        session.setConfig(config);
        session.setUserInfo(ui);
        setProxy(session);
        session.connect();

//        if (connectionProperties.containsKey(getName(INIT_COMMAND))) {
//            executeInitCommand(session, get(INIT_COMMAND));
//        }

        return session;
    }


//    private static void executeInitCommand(final Session session, final String command) throws JSchException {
//        System.out.println("SSH: " + command);
//        final ChannelExec channel = (ChannelExec) session.openChannel(
//                "exec");
//        channel.setCommand(command);
//        channel.connect();
//        try {
//            System.out.println(IOUtils.toString(channel.getInputStream()));
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        System.out.println("Exit status:" + channel.getExitStatus());
//        channel.disconnect();
//    }

    private void setProxy(final Session session) {
        if (Objects.nonNull(config.getProxyHost())) {
            session.setProxy(new ProxyHTTP(config.getProxyHost(), config.getProxyPort()));
        }
    }

}
