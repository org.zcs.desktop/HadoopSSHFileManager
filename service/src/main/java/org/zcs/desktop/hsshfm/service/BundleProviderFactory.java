package org.zcs.desktop.hsshfm.service;

import com.google.inject.Inject;
import org.zcs.desktop.hsshfm.config.ConnectionNames;
import org.zcs.desktop.hsshfm.model.BundleProvider;
import org.zcs.desktop.hsshfm.model.FileSystemType;
import org.zcs.desktop.hsshfm.service.hadoop.HadoopBundleProvider;
import org.zcs.desktop.hsshfm.service.local.LocalBundleProvider;
import org.zcs.desktop.hsshfm.service.ssh.SSHBundleProvider;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class BundleProviderFactory {
	private final Map<String, BundleProvider> fileSystemProviderMap = new HashMap<>();
	private final ConnectionNames connectionNames;

	@Inject
	public BundleProviderFactory(final ConnectionNames connectionNames) {
		this.connectionNames = connectionNames;
	}

	public BundleProvider getProvider(final String connectionName) throws IOException {
		BundleProvider result = fileSystemProviderMap.get(connectionName);
		if (result == null) {
			result = createBundleProvider(connectionName);
			fileSystemProviderMap.put(connectionName, result);
		}
		return result;
	}

	private BundleProvider createBundleProvider(final String connectionName) throws IOException {
		switch (connectionNames.getType(connectionName)) {
		case LOCAL:
			return new LocalBundleProvider();
		case SSH:
			return new SSHBundleProvider(connectionNames.getSSHConnectionConfig(connectionName));
		case HADOOP:
			return new HadoopBundleProvider(connectionNames.getHDFSConnectionConfig(connectionName));
		}
		return null;
	}

	public void close() {
		for (Map.Entry<String, BundleProvider> current : fileSystemProviderMap
				.entrySet()) {
			try {
				current.getValue().close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}
	
	public BundleProvider getDefaultProvider() throws IOException {
		return getProvider(FileSystemType.LOCAL.toString());
	}

}
